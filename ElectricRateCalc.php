<html>
    <body>
        <form action = " " method = "POST">
        <h1>Calculate</h1>     
            <form method="post" action="">
                <label for="voltage">Voltage</label><br>
                <input type="text" class="form-control" id="voltage" name="voltage"> 
                <br><i>Voltage (V)</i><br>
                
                <br><label for="current">Current</label><br>
                <input type="text" class="form-control" id="current" name="current">
                <br><i>Ampere (A)</i><br> 

                <br><label for="currentRate">Current Rate</label><br>
                <input type="text" class="form-control" id="currentRate" name="currentRate">
                <br><i>sen/kWh</i><br> 

                <br><button type="calculate" class="btn btn-primary" name="btn"> Calculate</button>
                </form><br>

            <?php
                //Power (Wh) = Voltage (V) * Current  (A);
                //Energy (kWh) = Power * Hour * 1000;
                //Total = Energy(kWh) * (current rate/100);

                //power:0.06156kw
                //rate:0.218RM
                if(isset($_POST["btn"]))
                {
                        $voltage = $_POST['voltage'];
                        $current = $_POST['current'];
                        $currentRate = $_POST['currentRate'];
                        $tariff = 0;


                        if($voltage <= 0.05)
                        {
                            $power = $voltage * $current;
                        }
                        else if($voltage > 0.05 && $voltage <= 1)
                        {
                            $power = $voltage * $current;
                        }
                        else if($voltage > 1 && $voltage <= 50)
                        {
                            $power = $voltage * $current;
                        }
                        else if($voltage > 50 && $voltage <= 230)
                        {
                            $power = $voltage * $current;
                        }
                        else
                        {
                            $power = $voltage * $current;
                        }


                        if($tariff >= 1 && $tariff <= 200)
                        {
                            $energy = $power * 1 * 1000;

                        }
                        else if($tariff > 200 && $tariff <= 300)
                        {
                            $energy = $power * 1 * 1000;

                        }
                        else if($tariff > 300 && $tariff <= 600)
                        {
                            $energy = $power * 1 * 1000;

                        }
                        else if($tariff > 600 && $tariff <= 900)
                        {
                            $energy = $power * 1 * 1000;

                        }
                        else
                        {
                            $energy = $power * 1 * 1000;

                        }


                        if($tariff <= 1 && $tariff <= 200)
                        {
                            $total = $energy * ($currentRate/100);

                        }
                        else if($tariff > 200 && $tariff <= 300)
                        {
                            $total = $energy * ($currentRate/100);

                        }
                        else if($tariff > 300 && $tariff <= 600)
                        {
                            $total = $energy * ($currentRate/100);

                        }
                        else if($tariff > 600 && $tariff <= 900)
                        {
                            $total = $energy * ($currentRate/100);

                        }
                        else
                        {
                            $total = $energy * ($currentRate/100);

                        }
                        
                        //calculate electricity rates per hour
                        //calculate electricity per day based on user input of voltage, current (A), and current rate
                        echo "Electricity rates per hour: " . number_format($total, 2)." kWh" ."</br>";

                        $costPerDay = $total * 24;
                        echo "Electricity rates per day: " . number_format($costPerDay, 2)." kWh" ."</br>";
                    //}
                    
  
                }
        ?>                
    </body>
</html>